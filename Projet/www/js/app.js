// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })

  .state('app.lampe', {
    url: '/lampe',
    views: {
      'menuContent': {
        templateUrl: 'templates/lampe.html',
        controller: 'LampeCtrl'
      }
    }
  })

  .state('app.vibration', {
    url: '/vibre',
    views: {
      'menuContent': {
        templateUrl: 'templates/vibre.html',
        controller: 'VibreCtrl'
      }
    }
  })

  .state('app.statusbar', {
    url: '/statusbar',
    views: {
      'menuContent': {
        templateUrl: 'templates/Statusbar.html',
        controller: 'StatusbarCtrl'
      }
    }
  })

  .state('app.epileptique', {
    url: '/epileptique',
    views: {
      'menuContent': {
        templateUrl: 'templates/epileptique.html',
        controller: 'EpileptiCtrl'
      }
    }
  });

  $urlRouterProvider.otherwise('/app/home');
});
