angular.module('starter.controllers', [])

.controller('AppCtrl', function() {})

.controller('LampeCtrl', function($scope) {
  $scope.switchLight = function() {
    // console.log("TURN UP THE LIIIIIIIIIIIIGHT");
    window.plugins.flashlight.toggle();
  };
})

.controller('VibreCtrl', function($scope) {
  $scope.optionMassage = function() {
    // console.log("Vibreur activé pour 3 sec.");
    navigator.vibrate(3000);
  };
})

.controller('StatusbarCtrl', function($scope) {
  $scope.ChangeColor = function() {
    var a = ["black", "darkGray", "lightGray", "white", "gray", "red", "green", "blue", "cyan", "yellow", "magenta", "orange", "purple", "brown"];
    StatusBar.backgroundColorByName(a[randomNumber(0, 14)]);
  };
  $scope.ResetColor = function() {
    StatusBar.backgroundColorByName("black");
  };
})

.controller('EpileptiCtrl', function($scope, $interval) {
  $scope.Epilepsie = function() {
    var my_media = null;
    playAudio("/android_asset/www/sounds/musique1.wav");

    var inter = $interval(function() {
      window.plugins.flashlight.toggle();
    }, 50, 100); // 50 * 100 pour tenir 5 secondes

    var inter2 = $interval(function() {
      navigator.vibrate(randomNumber(100, 200));
    }, 200, 25); // 25 * 200 pour tenir 5 secondes

    var inter3 = $interval(function() {
      var a = ["black", "darkGray", "lightGray", "white", "gray", "red", "green", "blue", "cyan", "yellow", "magenta", "orange", "purple", "brown"];
      StatusBar.backgroundColorByName(a[randomNumber(0, 14)]);
      // console.log("Nouvelle couleur : " + a[randomNumber(0, 14)]);
      document.getElementById("test").style.backgroundColor = a[randomNumber(0, 14)];
      // console.log("document.getElementById(\"test\").style.backgroundColor = \"black\";");
    }, 50, 100); // 50 * 100 pour tenir 5 secondes

    setTimeout(function(){ resetScreen(); }, 5200); // reset les modifications après 5.2 secondes
  };
});

function resetScreen() {
  StatusBar.backgroundColorByName("black");
  window.plugins.flashlight.switchOff();
  document.getElementById("test").style.backgroundColor = "inherit";
  my_media.stop();
};

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max-min)) + min;
}

function playAudio(src) {
  var my_media = new Media(src);
  var VolumeControl = cordova.plugins.VolumeControl;
  VolumeControl.setVolume(1.0);
  my_media.play();
}

function getPhoneGapPath() {
  var path = window.location.pathname;
  path = path.substr( path, path.length - 10 );
  return path;
};
